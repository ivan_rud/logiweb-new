<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ivanrud
  Date: 29.04.2021
  Time: 21:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>IRC | Homepage</title>

    <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.ico?" type="image/x-icon"/>
    <style>
        <%@include file="/pages/styles/style.css" %>
    </style>
</head>
<body style="font-family: 'Montserrat'; font-weight: bolder">

<div class="head">
    <img src="${pageContext.request.contextPath}/images/mainlogo.png" width="118px" height="35px"
         style="margin-top: 17px; margin-left: 75px"/>
</div>

<div class="wrapper">
    <div class="menu">
        <a href="cargos">
            <button class="menu-button">
                📦&nbsp;&nbsp;&nbsp;Orders
            </button>
        </a>
        <br>
        <a href="trucks">
            <button class="menu-button">
                🚚&nbsp;&nbsp;&nbsp;Trucks
            </button>
        </a>
        <br>
        <a href="drivers">
            <button class="menu-button">
                👦🏻&nbsp;&nbsp;&nbsp;Drivers
            </button>
        </a>
        <br>
        <a href="cities">
            <button class="menu-button">
                🏙️&nbsp;&nbsp;&nbsp;Cities
            </button>
        </a>
    </div>
    <div class="content">

    </div>
</div>
</body>
</html>
