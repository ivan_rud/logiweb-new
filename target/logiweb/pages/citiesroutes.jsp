<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: ivanrud
  Date: 04.05.2021
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>IRC | Cities</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.ico?" type="image/x-icon"/>

    <style>
        <%@include file="/pages/styles/style.css" %>

    </style>
</head>
<body style="font-family: 'Helvetica'">

<div class="head">
    <img src="${pageContext.request.contextPath}/images/mainlogo.png" width="118px" height="35px"
         style="margin-top: 17px; margin-left: 75px"/>
</div>

<div class="wrapper">
    <div class="menu">
        <a href="cargos">
            <button class="menu-button">📦&nbsp;&nbsp;&nbsp;Orders</button>
        </a>
        <br>
        <a href="trucks">
            <button class="menu-button" href="trucks">🚚&nbsp;&nbsp;&nbsp;Trucks</button>
        </a>
        <a href="drivers">
            <button class="menu-button">
                👦🏻&nbsp;&nbsp;&nbsp;Drivers
            </button>
        </a>
        <button class="menu-button-clicked">
            🏙️&nbsp;&nbsp;&nbsp;Cities
        </button>
    </div>
    <div class="content">
        <div style="width: 600px; margin: 50px auto auto;">
            <h2 style="text-align: center; margin-bottom: 30px">🏙️</h2>

            <button class="btn btn-primary" data-toggle="modal" data-target="#addModal"
                    style="background-color: #4460ef; font-weight: bold; width: 400px; float: left;">+
            </button>

            <button class="btn btn-primary" data-toggle="modal" data-target="#newCity"
                    style="background-color: #2a9d8f; font-weight: bold; width: 180px; float: right">New city
            </button>

            <div id="newCity" class="modal fade" style="z-index: 1300 !important;">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 600px; padding: 20px">
                        <h2 style="margin-bottom: 30px">Adding a new city</h2>
                        <form:form action="cities/newcity" method="post" modelAttribute="newCity">
                            <div class="form-group row">
                                <h2 for="cityInput" class="col-sm-2 col-form-label">City title: </h2>
                                <div class="col-sm-10">
                                    <form:input path="title" type="text" class="form-control" id="cityInput"
                                                placeholder="City title"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary" style="width: 560px">Add</button>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>

            <div id="addModal" class="modal fade" style="z-index: 1300 !important;">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 600px; padding: 20px">
                        <h2 style="margin-bottom: 30px">Adding a new route</h2>

                        <form:form action="cities/add" method="post" modelAttribute="newRoute">
                            <div class="form-group row">
                                <h2 for="depInput" class="col-sm-2 col-form-label">From: </h2>
                                <div class="col-sm-10">
                                    <form:select path="departure" type="text" class="form-control" id="depInput">
                                        <c:forEach items="${cities}" var="city">
                                            <form:option value="${city.title}">${city.title}</form:option>
                                        </c:forEach>
                                    </form:select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <h2 for="destInput" class="col-sm-2 col-form-label">To: </h2>
                                <div class="col-sm-10">
                                    <form:select path="destination" type="text" class="form-control" id="destInput">
                                        <c:forEach items="${cities}" var="city">
                                            <form:option value="${city.title}">${city.title}</form:option>
                                        </c:forEach>
                                    </form:select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <h2 for="distanceInput" class="col-sm-2 col-form-label">Distance (.km): </h2>
                                <div class="col-sm-10">
                                    <form:input path="distance" type="text" class="form-control" id="distanceInput"
                                                placeholder="Distance"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary" style="width: 560px">Add</button>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>

            <table class="table" style="margin: 20px auto auto; font-family: 'Montserrat'">
                <thead style="background-color: #2b2d42; color: white">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">FROM</th>
                    <th scope="col">TO</th>
                    <th scope="col">DISTANCE (.km)</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${routes}" var="route">
                    <tr>
                        <th scope="row" style="color: #4460ef">${route.id}</th>
                        <td>${route.departure}</td>
                        <td>${route.destination}</td>
                        <td>${route.distance}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
                crossorigin="anonymous"></script>
    </div>
</div>
</body>
</html>
