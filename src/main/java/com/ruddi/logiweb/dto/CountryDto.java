package com.ruddi.logiweb.dto;

import com.ruddi.logiweb.model.City;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Setter
@Getter
public class CountryDto {
    private int id;
    private String departure;
    private String destination;
    private int distance;
}
