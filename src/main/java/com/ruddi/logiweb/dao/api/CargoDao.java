package com.ruddi.logiweb.dao.api;

import com.ruddi.logiweb.model.Cargo;

public interface CargoDao extends GenericDao<Cargo>{
}
