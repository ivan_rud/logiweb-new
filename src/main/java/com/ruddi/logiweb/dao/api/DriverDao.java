package com.ruddi.logiweb.dao.api;

import com.ruddi.logiweb.model.Driver;

public interface DriverDao extends GenericDao<Driver> {
}
