package com.ruddi.logiweb.dao.impl;

import com.ruddi.logiweb.dao.api.DriverDao;
import com.ruddi.logiweb.model.Driver;
import org.springframework.stereotype.Repository;

@Repository
public class DriverDaoImpl extends GenericDaoImpl<Driver> implements DriverDao {
    public DriverDaoImpl() {
        init(Driver.class);
    }
}
