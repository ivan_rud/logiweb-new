package com.ruddi.logiweb.dao.impl;

import com.ruddi.logiweb.dao.api.CargoDao;
import com.ruddi.logiweb.model.Cargo;
import org.springframework.stereotype.Repository;

@Repository
public class CargoDaoImpl extends GenericDaoImpl<Cargo> implements CargoDao {
    public CargoDaoImpl() {
        init(Cargo.class);
    }
}
