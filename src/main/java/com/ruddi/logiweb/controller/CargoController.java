package com.ruddi.logiweb.controller;

import com.ruddi.logiweb.dto.CargoDto;
import com.ruddi.logiweb.model.City;
import com.ruddi.logiweb.service.api.CargoService;
import com.ruddi.logiweb.service.api.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CargoController {

    @Autowired
    CargoService cargoService;

    @Autowired
    CityService cityService;

    @GetMapping("/cargos")
    public String cargoStart(Model model) {

        model.addAttribute("cargoslist", cargoService.getAll());
        model.addAttribute("newCargo", new CargoDto());
        model.addAttribute("cities", cityService.getAll());

        return "cargosinfo";
    }

    @GetMapping("/cargos/delete")
    public String cargoDelete(@RequestParam int id) {
        cargoService.remove(id);
        return "redirect:/cargos";
    }

    @PostMapping("/cargos/add")
    public String addCargo(@ModelAttribute CargoDto cargo) {
        cargoService.save(cargo);
        return "redirect:/cargos";
    }

    @PostMapping("/cargos/edit")
    public String editCargo(@ModelAttribute CargoDto cargo) {
        cargoService.update(cargo);
        return "redirect:/cargos";
    }
}
