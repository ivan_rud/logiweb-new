package com.ruddi.logiweb.controller;

import com.ruddi.logiweb.dto.DriverDto;
import com.ruddi.logiweb.dto.TruckDto;
import com.ruddi.logiweb.model.City;
import com.ruddi.logiweb.model.Truck;
import com.ruddi.logiweb.service.api.CityService;
import com.ruddi.logiweb.service.api.DriverService;
import com.ruddi.logiweb.service.api.TruckService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DriverController {

    @Autowired
    DriverService driverService;

    @Autowired
    CityService cityService;

    @GetMapping("/drivers")
    public String driverStart(Model model) {
        model.addAttribute("driversList", driverService.getAll());
        model.addAttribute("newDriver", new DriverDto());
        model.addAttribute("cities", cityService.getAll());
        return "driversinfo";
    }

    @GetMapping("/drivers/delete")
    public String deleteDriver(@RequestParam int id) {
        driverService.remove(id);
        return "redirect:/drivers";
    }

    @PostMapping("/drivers/add")
    public String addDriver(@ModelAttribute DriverDto driver) {

        driverService.save(driver);

        return "redirect:/drivers";
    }

    @PostMapping("/drivers/edit")
    public String editDriver(@ModelAttribute DriverDto driver) {

        driverService.update(driver);

        return "redirect:/drivers";
    }
}
