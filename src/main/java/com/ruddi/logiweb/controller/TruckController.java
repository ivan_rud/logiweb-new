package com.ruddi.logiweb.controller;

import com.ruddi.logiweb.dto.TruckDto;
import com.ruddi.logiweb.model.City;
import com.ruddi.logiweb.service.api.CityService;
import com.ruddi.logiweb.service.api.TruckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TruckController {

    @Autowired
    TruckService truckService;

    @Autowired
    CityService cityService;

    @GetMapping("/trucks")
    public String trucksStart(Model model) {
        model.addAttribute("truckslist", truckService.getAll());
        model.addAttribute("newTruck", new TruckDto());
        model.addAttribute("cities", cityService.getAll());

        return "trucksinfo";
    }

    @GetMapping("/trucks/delete")
    public String truckDelete(@RequestParam int id) {
        truckService.remove(id);
        return "redirect:/trucks";
    }

    @PostMapping("/trucks/add")
    public String addTruck(@ModelAttribute TruckDto truck) {
        truckService.save(truck);
        return "redirect:/trucks";
    }

    @PostMapping("/trucks/edit")
    public String editTruck(@ModelAttribute TruckDto truck) {
        truckService.update(truck);
        return "redirect:/trucks";
    }
}
