package com.ruddi.logiweb.controller;

import com.ruddi.logiweb.dto.CityDto;
import com.ruddi.logiweb.dto.CountryDto;
import com.ruddi.logiweb.model.City;
import com.ruddi.logiweb.service.api.CityService;
import com.ruddi.logiweb.service.api.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class CountryController {

    @Autowired
    CountryService countryService;

    @Autowired
    CityService cityService;

    @GetMapping("/cities")
    public String citiesStart(Model model) {

        model.addAttribute("routes", countryService.getAll());
        model.addAttribute("cities", cityService.getAll());
        model.addAttribute("newCity", new CityDto());
        model.addAttribute("newRoute", new CountryDto());

        return "citiesroutes";
    }

    @PostMapping("/cities/add")
    public String citiesAdd(@ModelAttribute CountryDto country) {
        countryService.save(country);

        return "redirect:/cities";
    }

    @PostMapping("/cities/newcity")
    public String cityAdd(@ModelAttribute CityDto city) {

        cityService.save(city);

        return "redirect:/cities";
    }
}
