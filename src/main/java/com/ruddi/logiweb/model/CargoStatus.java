package com.ruddi.logiweb.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum CargoStatus {
    PREPARED("Prepared for dispatch"),
    DISPATCHED("Dispatched"),
    DELIEVERED("Delievered");

    @Getter
    private final String status;
};
