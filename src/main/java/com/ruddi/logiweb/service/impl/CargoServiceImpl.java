package com.ruddi.logiweb.service.impl;

import com.ruddi.logiweb.dao.api.CargoDao;
import com.ruddi.logiweb.dto.CargoDto;
import com.ruddi.logiweb.model.Cargo;
import com.ruddi.logiweb.service.api.CargoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CargoServiceImpl extends GenericServiceImpl<Cargo, CargoDto, CargoDao> implements CargoService {
    @Autowired
    public CargoServiceImpl(CargoDao dao, ModelMapper mapper) {
        super(Cargo.class, CargoDto.class, dao, mapper);
    }
}
