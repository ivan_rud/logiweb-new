package com.ruddi.logiweb.service.impl;

import com.ruddi.logiweb.dao.api.GenericDao;
import com.ruddi.logiweb.service.api.GenericService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@NoArgsConstructor
public class GenericServiceImpl<T, DT, D extends GenericDao<T>> implements GenericService<T, DT> {
    private Class<T> persistentClass;
    private Class<DT> persistentDtoClass;

    D dao;
    ModelMapper mapper;

    @Override
    @Transactional
    public void save(DT entity) {
        dao.save(mapper.map(entity, persistentClass));
    }

    @Override
    @Transactional
    public void update(DT entity) {
        dao.update(mapper.map(entity, persistentClass));
    }

    @Override
    @Transactional
    public void remove(DT entity) {
        dao.remove(mapper.map(entity, persistentClass));
    }

    @Override
    @Transactional
    public void remove(int id) {
        dao.remove(id);
    }

    @Override
    @Transactional
    public DT find(int id) {
        return mapper.map(dao.find(id), persistentDtoClass);
    }

    @Override
    @Transactional
    public List<DT> getAll() {
        return dao.getAll().stream().map(entity ->
                mapper.map(entity, persistentDtoClass)).collect(Collectors.toList());
    }
}
