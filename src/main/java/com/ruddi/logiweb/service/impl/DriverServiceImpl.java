package com.ruddi.logiweb.service.impl;

import com.ruddi.logiweb.dao.api.DriverDao;
import com.ruddi.logiweb.dto.DriverDto;
import com.ruddi.logiweb.model.Driver;
import com.ruddi.logiweb.service.api.DriverService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DriverServiceImpl extends GenericServiceImpl<Driver, DriverDto, DriverDao> implements DriverService {
    @Autowired
    public DriverServiceImpl(DriverDao dao, ModelMapper mapper) {
        super(Driver.class, DriverDto.class, dao, mapper);
    }

}
