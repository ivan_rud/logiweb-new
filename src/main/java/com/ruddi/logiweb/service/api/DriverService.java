package com.ruddi.logiweb.service.api;

import com.ruddi.logiweb.dto.DriverDto;
import com.ruddi.logiweb.model.Driver;

public interface DriverService extends GenericService<Driver, DriverDto> {
}
