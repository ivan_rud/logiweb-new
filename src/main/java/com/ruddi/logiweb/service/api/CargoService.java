package com.ruddi.logiweb.service.api;

import com.ruddi.logiweb.dto.CargoDto;
import com.ruddi.logiweb.model.Cargo;

public interface CargoService extends GenericService<Cargo, CargoDto>{
}
